var info = JSON.parse(data);

replaceTitle();
replaceContent();

function replaceTitle()
{
  document.getElementById("row2-info2-title").innerHTML = info.title;
}

function replaceContent()
{
  document.getElementById("row2-info2-content").innerHTML = info.content;
}
