<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>NASA - news upload</title>
    <link rel="stylesheet" href="../Style/adminStyle.css">
  </head>
  <body>
    <?php
      $path = dirname(__FILE__)."/../resources/uploads/";
      $path = $path . basename( $_FILES['fileToUpload']['name']);
      $dirPath = dirname(__FILE__)."/../resources/uploads/";

      if (empty($_POST['submit']))
      {
        die();
      }

      if (checkLegitimateFormat() == false)
      {
        echo "Bad format. Upload failure.";
        die();
      }

      if (move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $dirPath . "testtest.png"))
      {
        $content = "data = '{\"title\" : \"" . $_POST['title'] . "\", \"content\" : \"" . $_POST['content'] . "\"}';";
        $fp = fopen($dirPath . "data.json","wb");
        fwrite($fp,$content);
        fclose($fp);

        $message = "the file". basename( $_FILES['fileToUpload']['name'])." has been uploaded";
        echo "<script type='text/javascript'>
        alert('$message');
        window.location.href = \"/../index.html\";
        </script>";
      }
      else
      {
        if ($_FILES["fileToUpload"]["error"] == UPLOAD_ERR_INI_SIZE )
        {
          $message = "File too large";
          echo "<script type='text/javascript'>
          alert('$message');
          window.location.href = \"admin.php\";
          </script>";
        }
        else
        {
          echo "undefined error", $_FILES["fileToUpload"]["error"];
        }
      }

      function checkLegitimateFormat()
      {
        $fileName = $_FILES['fileToUpload']['name'];
        $legitimate_extensions = array('jpeg', 'jpg', 'png', 'bmp');
        $fileExtension = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));

        if (in_array($fileExtension, $legitimate_extensions))
        {
          return true;
        }
        return false;
      }

      function checkIfFileExists($path)
      {

        if (file_exists($path))
        {
          return true;
        }
        return false;
      }
    ?>
  </body>
</html>
