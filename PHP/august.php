<?php

$files = array_filter(scandir('../resources/row1/'), function($item) {
    return !is_dir('../resources/row1/' . $item);
});

if (isset($_GET['file'])) {
  $abc = $_GET['file'];
}
else{
  die();
}
$match = $files[array_keys($files)[(int)$abc]];

header('Content-type: image/png');
header('Content-Disposition: attachment; filename="' . $abc .".". strtolower(pathinfo($match, PATHINFO_EXTENSION)) . '"');
echo file_get_contents("../resources/row1/" . $match);

?>
