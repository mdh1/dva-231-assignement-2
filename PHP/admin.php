<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../Style/adminStyle.css">
    <title>NASA - admin</title>
  </head>
  <body>
    <div class="header">
        <a href="../index.html">
          <img class="headerImage" src="../resources/header/logo.png">
        </a>
    </div>

    <div class="uploadForm">
      <form action="upload.php" method="POST" enctype="multipart/form-data">
        <br><input type="text" name="title" placeholder="Title"><br><br>
        <textarea type="text" name="content" placeholder="Information about the picture"></textarea><br><br>
        <br><input type="file" name="fileToUpload" id="fileToUpload"><br><br>
        <input type="submit" name="submit" value="Ladda upp"></input>
      </form>
    </div>
  </body>
</html>
