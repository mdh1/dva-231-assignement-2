<?php

session_start();

if ($_SESSION['loggedIn']) {
  echo "<script type='text/javascript'>
  window.location.href = \"admin.php\";
  </script>";
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../Style/loginStyle.css">
    <title>NASA - login</title>
  </head>
  <body>
    <div class="header">
        <a href="../index.html">
          <img class="headerImage" src="../resources/header/logo.png">
        </a>
    </div>

    <div class="loginForm">
      <form action="login.php" method="POST" enctype="multipart/form-data">
        <br><input type="text" name="username" placeholder="Username"><br><br>
        <input type="text" name="password" placeholder="Password"></input><br><br>
        <input type="submit" name="submit" value="Log in"></input>
      </form>
    </div>
  </body>
</html>

<?php
  if (isset($_POST['submit'])) {
    $_SESSION['loggedIn'] = true;
    echo "<script type='text/javascript'>
    window.location.href = \"admin.php\";
    </script>";
  }
 ?>
