<?php

session_start();

if (session_status() != PHP_SESSION_ACTIVE) {
    $_SESSION["currentImage"] = 0;
}

$files = array_filter(scandir('../resources/row1/'), function($item) {
    return !is_dir('../resources/row1/' . $item);
});

$amountOfImages = sizeof($files);

if (isset($_POST)) {
  header('Content-type: text/plain');
  echo $_SESSION["currentImage"]++;

  if ($_SESSION["currentImage"] == $amountOfImages)
  {
    $_SESSION["currentImage"] = 0;
  }
}
?>
